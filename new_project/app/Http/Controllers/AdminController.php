<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\User;

class AdminController extends Controller
{
    //
    public function index()
    {
        $users = Auth::User();
        return view('admin.index' , compact('users'));
    }

    public function show(){

        $users = User::all();

        return view('admin.users.show' , ['users'=>$users]);

    }

    public function delete($id){

        $user = User::FindorFail($id);

        $user->delete();
        session()->flash('user_deleted' , "successfully deleted");
        return back();

    }
}
