<?php

namespace App\Http\Controllers;

use App\Models\Role;
use App\Models\User;
use Illuminate\Http\Request;

class RoleController extends Controller
{
    //

    public function create()
    {
        $roles = Role::all();
        return view('admin.roles.create-role' , ['roles'=>$roles]);
    }

    public function store()
    {
        $inputs = request()->validate([

            'name' => 'required|min:2|max:20',
            'slug' => 'required|min:2|max:20'

        ]);
        Role::create($inputs);
        session()->flash('created_successfully');
        return redirect()->back();
    }

    public function show($id)
    {
        $user = User::find($id);
        $roles = Role::all();
        return view('admin.users.assign-role' , ['user'=>$user , 'roles'=>$roles]);
    }

    public function attach($id){

        $user = User::find($id);
        $role= Role::find(request('role_id'));

        $user->roles()->attach($role);

        return back();
    }

    public function detach($id){

        $user = User::find($id);
        $role= Role::find(request('role_id'));

        $user->roles()->detach($role);

        return back();
    }
    public function edit($id)
    {
        $role = Role::find($id);
        return view('admin.roles.edit' , ['role'=>$role]);
    }
    public function update($id)
    {
        $role = Role::find($id);
        $role->name = \request('name');
        $role->slug = \request('slug');

        $role->save();
        session()->flash('role-updated' , 'updated successfully');

        return redirect()->route('roles.create');
    }

    public function delete($id)
    {
        $role = Role::find($id);
        $role->delete();
        session()->flash('role-deleted' , 'deleted successfully');

        return back();
    }
}
