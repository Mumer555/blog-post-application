<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    //

    public function show($id){

        $user = User::findorFail($id);
        return view('admin.admin-profile' , ['user'=>$user]);
    }

    public function update($id){
        $user = User::find($id);

        $inputs = request()->validate([
            'avatar' => 'file',
            'name' => 'required|min:1|max:255',
            'email' => 'required'
        ]);

        if (\request('avatar')){

            $inputs['avatar'] = \request('avatar')->store('images');
            $user->avatar = $inputs['avatar'];
        }

        if (\request('password') != null){

            $user->passowrd = \request('password');
        }

        $user->name = $inputs['name'];
        $user->email = $inputs['email'];

        $user->save();

        session()->flash('updated' , 'profile has been updated ');
        return redirect()->route('admin');


    }


}
