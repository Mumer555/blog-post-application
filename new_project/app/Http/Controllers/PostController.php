<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Post;
use App\Models\User;
use Illuminate\Support\Facades\Auth;


class PostController extends Controller
{
    //



    public function show($id){

        $posts = Post::find($id);
        return view('blog_post' , ['posts'=>$posts]);

    }

    public function create(){

        $users = Auth::User();
        return view('admin.posts.create' , ['users'=>$users]);

    }

    public function store(){

        $inputs = request()->validate([

            'title'=> 'required|min:10|max:255',
            'post_image' => 'file',
            'content' => 'required'
        ]);

        if (\request('post_image')){

            $inputs['post_image'] = \request('post_image')->store('images');
        }

        $users = Auth::User();

        $users->posts()->create($inputs);

        session()->flash('success' , 'post has been created ');

        return redirect()->route('posts.show');
    }

    public function index(){

        $users = Auth::User();
        $posts = Post::all();
        return view('admin.posts.showPost', ['users'=>$users , 'posts'=>$posts]);


    }

    public function delete($id){

        $posts = Post::findorFail($id);
        $this->authorize('delete' , $posts);
        $posts->delete();

        session()->flash('message' , 'post has been deleted ');


        return back();
    }

    public function edit($id){

        $posts = Post::findorFail($id);
        $users = Auth::User();

        $this->authorize('view' , $posts);
        return view('admin.posts.edit' , ['posts'=>$posts , 'users'=>$users]);



    }

    public function update($id){

        $posts = Post::findorFail($id);

        $inputs = request()->validate([
           'title'=>'required|min:10|max:255',
            'post_image'=>'file',
            'content'=>'required'
        ]);

        if (\request('post_image')){

            $inputs['post_image'] = \request('post_image')->store('images');
            $posts->post_image = $inputs['post_image'];

        }

        $posts->title = $inputs['title'];
        $posts->content = $inputs['content'];

        $this->authorize('update' , $posts);
        $posts->save();
        session()->flash('updated' , 'post has been updated ');
        return redirect()->route('posts.show');

    }

}
