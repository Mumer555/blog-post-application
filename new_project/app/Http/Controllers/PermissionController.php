<?php

namespace App\Http\Controllers;

use App\Models\Permission;
use Illuminate\Http\Request;
use App\Models\Role;

class PermissionController extends Controller
{
    //

    public function create(){

        $permissions = Permission::all();
        return view('admin.permission.create' , ['permissions'=>$permissions]);

    }

    public function store(){

        $input = request()->validate([

            'name' => 'required|min:4|max:255',
            'slug' => 'required'
        ]);

        Permission::create($input);
        session()->flash('permission-created');

        return back();

    }

    public function show($id){

        $role = Role::find($id);
        $permissions = Permission::all();

        return view('admin.permission.show' , ['role'=>$role , 'permissions'=>$permissions] );
    }

    public function attach($id){

        $role = Role::find($id);
        $permission = Permission::find(\request('permission'));

        $role->permissions()->attach($permission);

        session()->flash('permission-assigned');

        return back();

    }

    public function detach($id){

        $role = Role::find($id);
        $permission = Permission::find(\request('permission'));

        $role->permissions()->detach($permission);

        session()->flash('permission-detached');

        return back();

    }

    public function edit($id){

        $permission = Permission::find($id);

        return view('admin.permission.edit' , ['permission'=>$permission]);

    }

    public function update($id){

        $permission = Permission::find($id);

        $permission->name = request('name');
        $permission->slug = request('slug');

        $permission->save();

        session()->flash('permission-updated');

        return redirect()->route('admin.permissions.create');


    }

    public function delete($id){

        $permission = Permission::find($id);
        $permission->delete();
        session()->flash('deleted-permission');

        return back();
    }
}
