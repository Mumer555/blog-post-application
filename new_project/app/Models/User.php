<?php

namespace App\Models;

// use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function posts(){

        return $this->hasMany('App\Models\Post');

    }

    public function getAvatarAttribute($value){

        //********* For online images ***********************\\
        if( str_contains($value , 'https://') == true  || str_contains($value , 'http://') == true )
        {
        return $value;
        }

        //************* For Local Images ***********************\\

        return asset('storage/'.$value);
    }

    public function roles(){

        return $this->belongsToMany(Role::class);

    }

    public function permissions(){

        return $this->belongsToMany(Permission::class);

    }

    public function userIsAdmin($role_name){

        foreach ($this->roles as $role){
            if($role->name === $role_name)
            {
                return true;
            }
        }

        return false;

    }

}
