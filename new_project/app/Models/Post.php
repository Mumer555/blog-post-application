<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function user(){

        return $this->belongsTo('App\Models\User');

    }

//    public function setPostImageAttribute($value){
//
//        //********* For online images ***********************\\
//        if( str_contains($value , 'https://') == true  || str_contains($value , 'http://') == true )
//        {
//            $this->attributes['post_image'] = $value;
//        }
//
//        //************* For Local Images ***********************\\
//
//        $this->attributes['post_image'] = asset('storage/'.$value);
//
//    }

    public function getPostImageAttribute($value){

        //********* For online images ***********************\\
        if( str_contains($value , 'https://') == true  || str_contains($value , 'http://') == true )
        {
            return $value;
        }

        //************* For Local Images ***********************\\

        return asset('storage/'.$value);

    }

}
