<x-admin-master>

    @section('content')

        <hi>User Information</hi>

        <form>
            @csrf

            <div class="container py-4">

                <div class="img-profile">

                    <img height="100px" src="{{$user->avatar}}" alt="profile picture">

                </div>


                <div class="mb-3">
                    <label class="form-label" for="name">Name</label>
                    <input class="form-control" type="text" name="name" id="name" value="{{$user->name}}" disabled >


                </div>

                <div class="mb-3">
                    <label class="form-label" for="email">Email</label>
                    <input  class="form-control" type="text" name="email" id="email" value="{{$user->email}}" disabled>

                </div>


            </div>

            <!-- DataTales Example -->
            <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary">Posts Data</h6>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                            <thead>
                            <tr>
                                <th>Data</th>
                                <th>Id</th>
                                <th>Name</th>
                                <th>Slug</th>
                                <th>Attach</th>
                                <th>Detach</th>
                            </tr>
                            </thead>
                            <tfoot>
                            <tr>
                                <th>Data</th>
                                <th>Id</th>
                                <th>Name</th>
                                <th>Slug</th>
                                <th>Attach</th>
                                <th>Detach</th>
                            </tr>
                            </tfoot>
                            <tbody>
                            @foreach($roles as $role)
                                <tr>
                                    <td>
                                        <input type="checkbox"
                                               @foreach($user->roles as $user_role)
                                                   @if($user_role->name == $role->name)
                                                       checked
                                                   @endif
                                               @endforeach
                                        >
                                    </td>
                                    <td>{{$role->id}}</td>
                                    <td>{{$role->name}}</td>
                                    <td>{{$role->slug}}</td>

                                    <td>
                                        <form method="Post" action="{{route('admin.attach.role' , $user->id)}}" enctype="multipart/form-data">
                                            @csrf
                                            @method('PATCH')
                                            <input type="hidden" name="role_id" value="{{$role->id}}">
                                            <button class="btn btn-primary"  type="submit">Attach</button>

                                        </form>
                                    </td>
                                    <td>
                                        <form method="Post" action="{{route('admin.detach.role' , $user->id)}}" enctype="multipart/form-data">
                                            @csrf
                                            @method('PATCH')
                                            <input type="hidden" name="role_id" value="{{$role->id}}">
                                            <button class="btn btn-danger"  type="submit">Detach</button>

                                        </form>

                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </form>
    @endsection

        @section('scripts')
            <!-- Page level plugins -->
            <script src="{{asset('vendor/datatables/jquery.dataTables.min.js')}}"></script>
            <script src="{{asset('vendor/datatables/dataTables.bootstrap4.min.js')}}"></script>

            <!-- Page level custom scripts -->
            <script src="{{asset('js/demo/datatables-demo.js')}}"></script>
        @endsection


</x-admin-master>
