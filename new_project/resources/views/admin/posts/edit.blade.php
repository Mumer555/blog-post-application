<x-admin-master :users="$users">

    @section('content')

        <form  method="Post" action="{{route('posts.update' , $posts->id)}}" enctype="multipart/form-data">
            @csrf
            @method('PATCH')
            <div class="container py-4">

                <!--title data-->
                <div class="mb-3">
                    <label class="form-label" for="title">Title</label>
                    <input class="form-control" type="text" name="title" value="{{$posts->title}}">
                </div>

                <!--image file-->
                <div class="mb-3">
                    <div class="img-profile"><img height="100px" src="{{$posts->post_image}}" alt=""></div>
                    <label class="form-label" for="post_image">Choose Image</label>
                    <input class="form-control-file" type="file" name="post_image" >
                </div>
                <!--content data-->
                <div class="mb-3">
                    <label class="form-label" for="content">Content</label>
                    <textarea class="form-control" id="content" name="content" >{{$posts->content}}</textarea>
                </div>

                <!--Submit Button-->
                <dev class="d-grid">

                    <button class="btn btn-primary btn-lg" type="submit">Update</button>

                </dev>


            </div>


        </form>

    @endsection
</x-admin-master>
