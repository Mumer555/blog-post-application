<x-admin-master :users="$users">

    @section('content')

        <form method="Post" action="{{route('posts.store')}}" enctype="multipart/form-data">
            @csrf
            <!-- Wrapper container -->
            <div class="container py-4">


                    <!-- Title input -->
                    <div class="mb-3">
                        <label class="form-label" for="title">Title</label>
                        <input class="form-control" name="title" id="title" type="text" placeholder="Title" />
                    </div>

                    <!-- Image address input -->
                    <div class="mb-3">
                        <label class="form-label" for="post_image">Choose File</label>
                        <input class="form-control-file" name="post_image" id="post_image" type="file"  />
                    </div>

                    <!-- Content input -->
                    <div class="mb-3">
                        <label class="form-label" for="content">Content</label>
                        <textarea class="form-control" id="content" name="content" type="text" placeholder="content" style="height: 10rem;"></textarea>
                    </div>

                    <!-- Form submit button -->
                    <div class="d-grid">
                        <button class="btn btn-primary btn-lg" type="submit">Submit</button>
                    </div>


            </div>



        </form>
    @endsection

</x-admin-master>
