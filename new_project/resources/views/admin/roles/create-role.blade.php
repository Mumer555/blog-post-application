<x-admin-master>

    @section('content')
        <h3>Create Roles</h3>
        @if(session('role_updated'))
            <div classs="container p-5">
                <div class="row no-gutters">
                    <div class="col-lg-5 col-md-3">
                        <div class="alert alert-success fade show" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="True">&times;</span>
                            </button>
                            <h4 class="alert-heading">Success</h4>
                            <p>The role has been successfully created</p>
                        </div>
                    </div>
                </div>
            </div>
        @elseif(session('role_updated'))
            <div classs="container p-5">
                <div class="row no-gutters">
                    <div class="col-lg-5 col-md-3">
                        <div class="alert alert-success fade show" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="True">&times;</span>
                            </button>
                            <h4 class="alert-heading">Success</h4>
                            <p>The role has been successfully updated</p>
                        </div>
                    </div>
                </div>
            </div>
        @elseif(session('role_deleted'))
            <div classs="container p-5">
                <div class="row no-gutters">
                    <div class="col-lg-5 col-md-3">
                        <div class="alert alert-danger fade show" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="True">&times;</span>
                            </button>
                            <h4 class="alert-heading">Alert</h4>
                            <p>The role has been successfully deleted</p>
                        </div>
                    </div>
                </div>
            </div>
        @endif

        <div class="row">
                <form method="Post" action="{{route('roles.store')}}" enctype="multipart/form-data">
                    @csrf
                    <div class="container">
                        <div class="mb-2">
                            <label class="form-group" for="name" >Name</label>
                            <input class="form-control" type="text" name="name" placeholder="enter name of role">
                        </div>

                        <div class="mb-2">
                            <label class="form-group" for="slug" >Slug</label>
                            <input class="form-control" type="text" name="slug" placeholder="enter slug of role">
                        </div>

                        <div class="d-grid">
                            <button class="btn btn-primary btn-block" type="submit">Create</button>
                        </div>
                    </div>
                </form>

                <!-- DataTales Example -->
                <div class="col-9">
                    <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Roles Data</h6>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                        <tr>
                            <th>Id</th>
                            <th>Name</th>
                            <th>Slug</th>
                            <th>Update</th>
                            <th>Delete</th>
                        </tr>
                        </thead>
                        <tfoot>
                        <tr>
                            <th>Id</th>
                            <th>Name</th>
                            <th>Slug</th>
                            <th>Update</th>
                            <th>Delete</th>
                        </tr>
                        </tfoot>
                        <tbody>
                        @foreach($roles as $role)
                            <tr>
                                <td>{{$role->id}}</td>
                                <td><a href="{{route('assign.permissions.show' ,$role->id)}}">{{$role->name}}</a></td>
                                <td>{{$role->slug}}</td>

                                <td>
                                    <a href="{{route('admin.roles.edit' , $role->id)}}"><button class="btn btn-primary"  type="submit">Edit</button></a>
                                </td>
                                <td>
                                    <form method="Post" action="{{route('admin.role.delete' , $role->id)}}" enctype="multipart/form-data">
                                        @csrf
                                        @method('Delete')
                                        <button class="btn btn-danger"  type="submit">Delete</button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
                </div>
        </div>


    @endsection

    @section('scripts')
            <!-- Page level plugins -->
            <script src="{{asset('vendor/datatables/jquery.dataTables.min.js')}}"></script>
            <script src="{{asset('vendor/datatables/dataTables.bootstrap4.min.js')}}"></script>

            <!-- Page level custom scripts -->
            <script src="{{asset('js/demo/datatables-demo.js')}}"></script>
        @endsection

</x-admin-master>
