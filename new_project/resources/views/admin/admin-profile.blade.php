<x-admin-master>

    @section('content')

        <h3>Admin's profile</h3>

        <form method="Post" action="{{route('admin.profile.update' , $user->id)}}" enctype="multipart/form-data">
            @csrf
            @method('PATCH')

            <div class="container py-4">

                <div class="img-profile">

                    <img height="100px" src="{{$user->avatar}}" alt="profile picture">

                </div>

                <div class="mb-3">
                    <input class="form-control-file" type="file" name="avatar" >

                </div>

                <div class="mb-3">
                    <label class="form-label" for="name">Name</label>
                    <input class="form-control" type="text" name="name" id="name" value="{{$user->name}}">

                    @error('name')
                    <div class="alert alert-danger">{{$message}}</div>
                    @enderror

                </div>

                <div class="mb-3">
                    <label class="form-label" for="email">Email</label>
                    <input @error('email') is_invalid @enderror class="form-control" type="text" name="email" id="email" value="{{$user->email}}">
                    @error('email')
                    <div class="is_invalid alert-danger">{{$message}}</div>
                    @enderror
                </div>

                <div class="mb-3">
                    <label class="form-label" for="password">Password</label>
                    <input class="form-control" type="password" name="password" id="password" >
                </div>

                <div class="mb-3">
                    <label class="form-label" for="confirm-password">Confirm Password</label>
                    <input class="form-control" type="password" name="confirm_password" id="confirm-password" >
                </div>

                <div class="d-grid">
                    <button class="btn btn-primary btn-lg" type="submit">Update</button>
                </div>
            </div>



        </form>

    @endsection



</x-admin-master>
