<x-admin-master :users="$users">

    @section('content')
        @if(session('updated'))
            <div classs="container p-5">
                <div class="row no-gutters">
                    <div class="col-lg-5 col-md-12">
                        <div class="alert alert-success fade show" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="True">&times;</span>
                            </button>
                            <h4 class="alert-heading">Success</h4>
                            <p>The profile has been successfully updated</p>
                        </div>
                    </div>
                </div>
            </div>
        @endif

         <h1 class="h3 mb-4 text-gray-800">{{$users->name}}</h1>
    @endsection

        <x-admin-profile-master :users="$users"></x-admin-profile-master>

</x-admin-master>
