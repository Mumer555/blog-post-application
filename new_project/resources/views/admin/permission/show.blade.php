<x-admin-master>

    @section('content')
        @if(session('permission-assigned'))
            <div classs="container p-5">
                <div class="row no-gutters">
                    <div class="col-lg-5 col-md-3">
                        <div class="alert alert-success fade show" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="True">&times;</span>
                            </button>
                            <h4 class="alert-heading">Success</h4>
                            <p>The permission has been successfully assigned</p>
                        </div>
                    </div>
                </div>
            </div>
        @endif
            @if(session('permission-detached'))
                <div classs="container p-5">
                    <div class="row no-gutters">
                        <div class="col-lg-5 col-md-3">
                            <div class="alert alert-danger fade show" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="True">&times;</span>
                                </button>
                                <h4 class="alert-heading">Alert</h4>
                                <p>The permission has been successfully detached</p>
                            </div>
                        </div>
                    </div>
                </div>
            @endif
        <h1>assign permissions to role</h1>

        <div class="row">
            <form method="Post" action="" enctype="multipart/form-data">
                @csrf
                <div class="container">
                    <div class="mb-2">
                        <label class="form-group" for="name" >Name</label>
                        <input class="form-control" type="text" name="name" value="{{$role->name}}">
                    </div>

                    <div class="mb-2">
                        <label class="form-group" for="slug" >Slug</label>
                        <input class="form-control" type="text" name="slug" value="{{$role->slug}}">
                    </div>

                </div>
            </form>

        </div>
        <div class="row">
            <!-- DataTales Example -->
            <div class="col-lg-12">
                <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary">Permissions Table</h6>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                            <thead>
                            <tr>
                                <th>Options</th>
                                <th>Id</th>
                                <th>Name</th>
                                <th>Slug</th>
                                <th>Attach</th>
                                <th>Detach</th>
                            </tr>
                            </thead>
                            <tfoot>
                            <tr>
                                <th>Options</th>
                                <th>Id</th>
                                <th>Name</th>
                                <th>Slug</th>
                                <th>Attach</th>
                                <th>Detach</th>
                            </tr>
                            </tfoot>
                            <tbody>
                            @foreach($permissions as $permission)
                                <tr>
                                    <td><input type="checkbox"
                                        @foreach($role->permissions as $role_permission)
                                            @if($role_permission->name == $permission->name)
                                                checked
                                            @endif
                                        @endforeach
                                        >
                                    </td>
                                    <td>{{$permission->id}}</td>
                                    <td>{{$permission->name}}</td>
                                    <td>{{$permission->slug}}</td>
                                    <td>
                                        <form method="Post"  action="{{route('attach.role.to.permission' , $role->id)}}" enctype="multipart/form-data">
                                            @csrf
                                            @method('PATCH')
                                            <input type="hidden" name="permission" value="{{$permission->id}}">
                                            <button class="btn btn-primary" type="submit">Attach</button>
                                        </form>
                                    </td>
                                    <td>
                                        <form method="Post"  action="{{route('detach.role.to.permission' , $role->id)}}" enctype="multipart/form-data">
                                            @csrf
                                            @method('PATCH')
                                            <input type="hidden" name="permission" value="{{$permission->id}}">
                                            <button class="btn btn-danger" type="submit">Detach</button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            </div>
        </div>
    @endsection

    @section('scripts')
            <!-- Page level plugins -->
            <script src="{{asset('vendor/datatables/jquery.dataTables.min.js')}}"></script>
            <script src="{{asset('vendor/datatables/dataTables.bootstrap4.min.js')}}"></script>

            <!-- Page level custom scripts -->
            <script src="{{asset('js/demo/datatables-demo.js')}}"></script>
        @endsection

</x-admin-master>
