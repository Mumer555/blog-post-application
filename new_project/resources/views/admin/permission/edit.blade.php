<x-admin-master>

    @section('content')
        <form method="Post" action="{{route('admin.permissions.update' , $permission->id)}}" enctype="multipart/form-data">
            @csrf
            @method('PATCH')
            <div class="container py-4">

                <!--Name data-->
                <div class="mb-3">
                    <label class="form-label" for="name">Name</label>
                    <input class="form-control" type="text" name="name" value="{{$permission->name}}">
                </div>
                <!--Slug data-->
                <div class="mb-3">
                    <label class="form-label" for="Slug">Slug</label>
                    <input class="form-control" type="text" name="slug" value="{{$permission->slug}}">
                </div>

                <!--Submit Button-->
                <dev class="d-grid">
                    <button class="btn btn-primary btn-lg" type="submit">Update</button>
                </dev>


            </div>

        </form>

    @endsection

</x-admin-master>
