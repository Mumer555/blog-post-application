<x-post-master :posts="$posts">

    @section('content')

        <!-- Title -->
        <h1 class="mt-4">{{$posts->title}}</h1>

        <!-- Author -->
        <p class="lead">
            by
            <a href="{{route('admin')}}">{{$posts->user->name}}</a>
        </p>

        <hr>

        <!-- Date/Time -->
        <p>Posted {{$posts->created_at->diffForHumans()}}</p>

        <hr>

        <!-- Preview Image -->
        <img class="img-fluid rounded" src="{{$posts->post_image}}" alt="">

        <hr>

        <!-- Post Content -->
        <p>{{$posts->content}}</p>
        <blockquote class="blockquote">
            <footer class="blockquote-footer">Someone famous in
                <cite title="Source Title">{{$posts->title}}</cite>
            </footer>
        </blockquote>

        <hr>

        <!-- Comments Form -->
        <div class="card my-4">
            <h5 class="card-header">Leave a Comment:</h5>
            <div class="card-body">
                <form>
                    <div class="form-group">
                        <textarea class="form-control" rows="3"></textarea>
                    </div>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
            </div>
        </div>

        <!-- Single Comment -->
        <div class="media mb-4">
            <div class="media-body">
                <h5 class="mt-0">Hassan Hashmi</h5>
                Aaala keep it up brothers
            </div>
        </div>

        <!-- Comment with nested comments -->
        <div class="media mb-4">
            <div class="media-body">
                <h5 class="mt-0">Qamar Siddique</h5>
                    Mastii na karoo shoreyoo
                <div class="media mt-4">
                    <div class="media-body">
                        <h5 class="mt-0">Hurraira</h5>
                        i am hungary cum.....
                    </div>
                </div>


            </div>
        </div>

    @endsection



</x-post-master>
