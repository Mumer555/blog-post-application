<?php

use Illuminate\Support\Facades\Route;
use App\Models\Post;
use App\Models\User;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/insert/user/{id}/post' , function ($id){

    $post = new \App\Models\Post();
    $post->title = 'Business man ';
    $post->post_image = 'https://i.ytimg.com/vi/bfXZ1io4d3k/maxresdefault.jpg';
    $post->content = 'Garments business international brand pandu  ';

    $user = \App\Models\User::find($id);

    $user->posts()->save($post);
});

Route::get('/logout' , function (){

    return view('welcome');


})->name('logout');
Route::get('/dashboard', function () {

    return view('dashboard' );
})->middleware(['auth'])->name('dashboard');

Route::get('/', [ App\Http\Controllers\HomeController::class , 'show'])->name('home');

Route::get('/admin', 'App\Http\Controllers\AdminController@index')->name('admin');

Route::get('/post/{id}', 'App\Http\Controllers\PostController@show')->name('post');

Route::get('/admin/post/create', 'App\Http\Controllers\PostController@create')->name('posts.create');

Route::post('/admin/post/save', 'App\Http\Controllers\PostController@store')->name('posts.store');

Route::get('/show/posts', 'App\Http\Controllers\PostController@index')->name('posts.show');

Route::delete('/posts/{id}/destroy', 'App\Http\Controllers\PostController@delete')->name('posts.delete');

Route::get('/posts/{id}/show', 'App\Http\Controllers\PostController@edit')->name('posts.edit');

Route::patch('/posts/{id}/update', 'App\Http\Controllers\PostController@update')->name('posts.update');

Route::get('/admin/{id}/profile', 'App\Http\Controllers\UserController@show')->name('admin.profile.show');

Route::patch('/admin/profile/{id}/update', 'App\Http\Controllers\UserController@update')->name('admin.profile.update');



Route::delete('delete/user/{id}' , 'App\Http\Controllers\AdminController@delete')->name('admin.user.delete');
require __DIR__.'/auth.php';


Route::middleware('role:Admin')->group(function (){

    Route::get('show/all/users' , 'App\Http\Controllers\AdminController@show')->name('admin.users.show');

    Route::get('admin/roles/create' , 'App\Http\Controllers\RoleController@create')->name('roles.create');

    Route::post('admin/roles/store' , 'App\Http\Controllers\RoleController@store')->name('roles.store');

    Route::get('/assign/user/{id}/role', 'App\Http\Controllers\RoleController@show')->name('assign.roles');

    Route::Patch('attach/user/{id}/role' , 'App\Http\Controllers\RoleController@attach')->name('admin.attach.role');
    Route::Patch('detach/user/{id}/role' , 'App\Http\Controllers\RoleController@detach')->name('admin.detach.role');

    Route::get('edit/role/{id}' , 'App\Http\Controllers\RoleController@edit')->name('admin.roles.edit');

    Route::Patch('update/role/{id}' , 'App\Http\Controllers\RoleController@update')->name('admin.roles.update');
    Route::delete('delete/role/{id}' , 'App\Http\Controllers\RoleController@delete')->name('admin.role.delete');

    Route::get('assign/permission/role/{id}' , 'App\Http\Controllers\PermissionController@show')->name('assign.permissions.show');

    Route::patch('attach/role/{id}/toPermission' , 'App\Http\Controllers\PermissionController@attach')->name('attach.role.to.permission');
    Route::patch('detach/role/{id}/toPermission' , 'App\Http\Controllers\PermissionController@detach')->name('detach.role.to.permission');

    Route::get('create.permissions' , 'App\Http\Controllers\PermissionController@create')->name('admin.permissions.create');
    Route::post('store.permissions' , 'App\Http\Controllers\PermissionController@store')->name('permission.store');
    Route::delete('delete.permissions/{id}' , 'App\Http\Controllers\PermissionController@delete')->name('delete.permission');
    Route::get('edit/permission/{id}' , 'App\Http\Controllers\PermissionController@edit')->name('permission.edit');
    Route::patch('update/permission/{id}' , 'App\Http\Controllers\PermissionController@update')->name('admin.permissions.update');
});


